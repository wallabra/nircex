# Node IRC EX

is a Node.JS library that allows for a clean, async, Promises-based connection to IRC. It exposes a flexible,
powerful and extensible set of tools that allows for the easy creation of IRC message/event parsers and even
bots.

- [source code](https://gitlab.com/Gustavo6046/nircex.git)
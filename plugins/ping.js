// Test commands plugin.
module.exports = (nircex, bot) => ({
    commands: [
        {
            name: 'ping',
            callback: (conn, msg) =>
                conn.utility('message', msg.properties.target, `A pong for ${msg.properties.sourceNick}, two pongs for me!`)
        },
            
        {
            name: 'ucount',
            callback: (conn, msg) =>
                conn.utility('message', msg.properties.target, `${msg.properties.sourceNick}: There are ${conn.channelUsers[msg.properties.target].length} users in the current channel, and I have seen ${conn.allUsers.length} known users on the whole network.`)
        }
    ]
});
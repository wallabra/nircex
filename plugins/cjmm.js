// CJMM (Context-Jump Markov Model) implementation plugin.
const path = require('path');
const fs = require('fs');

delete require.cache[require.resolve('../cjmm')];
const $cjmm = require('../cjmm');

module.exports = (nircex, bot) => {
    let chainFn = path.join(bot.dbFolder, 'cjmm.json');
    let chains;

    function saveChains() {
        fs.writeFileSync(chainFn, JSON.stringify(chains));
    }

    function loadChains() {
        chains = JSON.parse(fs.readFileSync(chainFn, 'utf-8'));
    }

    if (fs.existsSync(chainFn))
        loadChains();

    else {
        chains = {};
        saveChains();
    }

    return {
        commands: [
            {
                name: 'cj',
                callback: (conn, msg) => {
                    let c;
                    
                    if (msg.args.length === 0)
                        c = chains['_'];
                        
                    else
                        c = chains[msg.args[0]];
                        
                    if (c == null)
                        return conn.utility('message', msg.properties.target, `No such chain, ${msg.properties.sourceNick}!`);

                    c = $cjmm(c);
                    let w = null;

                    if (msg.args.length > 1)
                        w = msg.args.slice(-1)[0];

                    let g = c.generate(w);

                    if (g != null)
                        return conn.utility('message', msg.properties.target, `${msg.properties.sourceNick}: "${msg.args.length > 2 ? msg.args.slice(1, -1).join(' ') + ' ' : ''}${g.join(' ')}"`);

                    else
                        return conn.utility('message', msg.properties.target, `Word generation returned null, ${msg.properties.sourceNick}. Try again?`);
                }
            },

            {
                name: 'cjparse',
                level: 250,
                callback: (conn, msg) => {
                    let targ = msg.args[0];
                    let fn = msg.args[1];
                    let newN = false;

                    let c = chains[targ];

                    if (c == null) {
                        c = $cjmm();
                        chains[targ] = c.data;
                        newN = true;
                    }

                    else c = $cjmm(c);

                    let lines = c.parseLines(fs.readFileSync(fn, 'utf-8'));
                    saveChains();

                    conn.utility('message', msg.properties.target, `${msg.properties.sourceNick}: Network ${targ} ${newN ? 'created' : 'fed'} ${lines} lines.`);
                }
            }
        ],

        handlers: [
            {
                predicates: [nircex.predicates.irc.publicMessage],
                callback: (conn, msg) => {
                    if (msg.content.startsWith(bot.prefix)) return;

                    let c = chains['_'];

                    if (c == null) {
                        c = $cjmm();
                        chains['_'] = c.data;
                    }

                    else c = $cjmm(c);

                    c.parseLines(msg.content);
                    saveChains();
                }
            }
        ]
    };
};

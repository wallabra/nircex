// A vague Context-Jump Markov Model implementation in Node.
let words = ['above-mentioned', 'above-listed', 'before-mentioned', 'aforementioned', 'abundance', 'accelerate', 'accentuate', 'accommodation', 'accompany', 'accomplish', 'accorded', 'accordingly', 'accrue', 'accurate', 'acquiesce', 'acquire', 'additional', 'address', 'addressees', 'adjustment', 'admissible', 'advantageous', 'advise', 'aggregate', 'aircraft', 'alleviate', 'allocate', 'alternatively', 'ameliorate', 'and/or', 'anticipate', 'applicant', 'application', 'apparent', 'apprehend', 'appreciable', 'appropriate', 'approximate', 'ascertain', 'attain', 'attempt', 'authorize', 'beg', 'belated', 'beneficial', 'bestow', 'beverage', 'capability', 'caveat', 'cease', 'chauffeur', 'clearly', 'obviously', 'combined', 'commence', 'complete', 'component', 'comprise', 'conceal', 'concerning', 'consequently', 'consolidate', 'constitutes', 'contains', 'convene', 'corridor', 'currently', 'deem', 'delete', 'demonstrate', 'depart', 'designate', 'desire', 'determine', 'disclose', 'different', 'discontinue', 'disseminate', 'duly', 'authorized', 'signed', 'each...apiece', 'economical', 'elect', 'eliminate', 'elucidate', 'emphasize', 'employ', 'encounter', 'endeavor', 'end', 'result', 'product', 'enquiry', 'ensure', 'entitlement', 'enumerate', 'equipments', 'equitable', 'equivalent', 'establish', 'evaluate', 'evidenced', 'evident', 'evince', 'excluding', 'exclusively', 'exhibit', 'expedite', 'expeditious', 'expend', 'expertise', 'expiration', 'facilitate', 'fauna', 'feasible', 'females', 'finalize', 'flora', 'following', 'forfeit', 'formulate', 'forward', 'frequently', 'function', 'furnish', 'grant', 'herein', 'heretofore', 'herewith', 'thereof', 'wherefore', 'wherein', 'however', 'identical', 'identify', 'immediately', 'impacted', 'implement', 'inasmuch', 'inception', 'indicate', 'indication', 'initial', 'initiate', 'interface', 'irregardless', 'liaison', '-ly', 'doubtless', 'fast', 'ill', 'much', 'seldom', 'thus', 'magnitude', 'maintain', 'majority', 'maximum', 'merge', 'methodology', 'minimize', 'minimum', 'modify', 'monitor', 'moreover', 'multiple', 'necessitate', 'nevertheless', 'notify', 'not...unless', 'not...except', 'not...until', 'notwithstanding', 'numerous', 'objective', 'obligate', 'observe', 'obtain', 'operate', 'optimum', 'option', 'orientate', '...out', 'calculate', 'cancel,', 'distribute', 'segregate', 'separate', 'overall', 'parameters', 'participate', 'particulars', 'perchance', 'perform', 'permit', 'perspire', 'peruse', 'place', 'portion', 'possess', 'potentiality', 'practicable', 'preclude', 'preowned', 'previously', 'prioritize', 'proceed', 'procure', 'proficiency', 'promulgate', 'provide', 'purchase', 'reflect', 'regarding', 'relocate', 'remain', 'remainder', 'remuneration', 'render', 'represents', 'request', 'require', 'requirement', 'reside', 'residence', 'respectively', 'retain', 'retire', 'rigorous', 'selection', 'separate', 'shall', 'solicit', 'state-of-the-art', 'strategize', 'subject', 'submit', 'subsequent', 'subsequently', 'substantial', 'sufficient', 'terminate', 'therefore', 'therein', 'timely', 'transpire', 'transmit', 'type', 'validate', 'variation', 'very', 'viable', 'warrant', 'whereas', 'whosoever', 'whomsoever', 'witnessed'];


function _choice(l) {    
    return l[Math.floor(l.length * Math.random())];
}

function _makeHumanID(size) {
    return `${new Array(size).fill(0).map(() => _choice(words)).join('-')}-${Math.floor(Math.random() * 10000)}`;
}

function $cjmm(data = []) {
    let cdata = data;

    let cw = {
        data: data,
        json: JSON.stringify.bind(JSON, data),
        length: data.length,
        id: _makeHumanID(3),


        parseLines: (text) => {
            return text.split('\n').map((l) => cw.teach(l.split(' '))).length;
        },
        
        containerContexts: (word) => {
            return data.map(cw.$context).filter((c) => c.has(word));
        },

        teach: (wordList) => {
            let cont = cw.$context();
            cont.teach(wordList);

            cw.data.push(cont.data);
            cw.length = cw.data.length;
        },

        generate: (word = null, jumpiness = undefined, maxSize = 250) => {
            let cont = null;

            if (word != null) {
                let containers = cw.containerContexts(word);

                if (containers.length === 0)
                    return null;

                cont = _choice(containers);
            }

            else {
                cont = cw.$context(_choice(data));
                word = _choice(cont.words);
            }

            let res = [];

            while (cont.has(word) && maxSize-- !== 0) {
                res.push(word);

                let _res = cont.randNext(word, jumpiness);

                if (_res == null)
                    return res;
                
                cont = _res.context;
                word = _res.word;
            }

            return res;
        },


        $context: function $context(data = {}) {
            let w = {
                data: data,
                json: JSON.stringify.bind(JSON, data),
                words: Object.keys(data),
                length: data.length,
                id: _makeHumanID(6),
        
        
                teach: (wordList) => {
                    wordList.slice(0, -1).forEach((a, ai) => {
                        let b = wordList[ai + 1];
                        w.addLink(a, b);
                    });
                },

                addLink: (from, to) => {
                    let _has = null;

                    if (data[from] != null) {
                        data[from].forEach((l) => {
                            if (l.next === to)
                                _has = l;
                        });
                    }

                    if (_has != null)
                        _has.weight++;

                    else {
                        _has = {
                            next: to,
                            weight: 1
                        };

                        data[from] = (data[from] != null ? data[from] : []).concat([_has]);
                    }

                    return _has;
                },

                randNext: (word, jumpiness = 0.7) => {
                    if (!w.has(word))
                        return null;
        
                    let res = {
                        word: null,
                        context: w
                    };
        
                    let possib = [];

                    data[word].forEach((l) => {
                        possib = possib.concat(new Array(l.weight).fill(l.next));
                    });

                    let nw = res.word = _choice(possib);
                    let inters = cdata.map(cw.$context).filter((c) => c.has(nw) && c.id !== w.id);

                    if (inters.length > 0 && Math.random() < jumpiness)
                        res.context = _choice(inters);

                    return res;
                },
        
                has: (word) => {
                    return w.words.indexOf(word) !== -1;
                }
            };
        
            return w;
        }
    };

    return cw;
}

$cjmm.fromConversations = function(convs) {
    let model = $cjmm();
    convs.forEach(model.teach);

    return model;
};

module.exports = $cjmm;